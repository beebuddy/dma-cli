import fs from 'fs';
import path from 'path';
import CallAPI from '../Util/network/CallAPI';
import chalk from 'chalk';
export default class UpdateManager {

    static async checkUpdate() {
        let fullPath = new URL(import.meta.url).pathname;
        let resolveFullPath = process.platform === "win32" ? fullPath.substr(1) : fullPath.substr(fullPath.indexOf("/"));

        const rootPrjDir = path.resolve(
            resolveFullPath,
            '../../../../'
        );

        const packageJsonFilePath = path.resolve(`${rootPrjDir}/package.json`);
        let packageJson = JSON.parse(fs.readFileSync(packageJsonFilePath));
        let { version, description } = packageJson
        console.log('%s', chalk.green.bold("[dma-cli] version : " + version));

        try {
            let result = await CallAPI.get(`https://gitlab.com/beebuddy/dma-cli/-/raw/main/package.json?inline=false`, { timeout: 1000, headers: { 'Cache-Control': 'no-cache' } });
            let jsonResult = result.data
            let lastestVersion = jsonResult.version;
            let lastestDescription = jsonResult.description;

            let notify = `
            ====================================================================
            There is a new version of dma-cli available (${lastestVersion}).               
            You are currently using dma-cli ${version}                                
            Install dma-cli globally using the package manager of your choice;   
            To get the latest version. You need to run command : 
                npm install -g git+https://gitlab.com/beebuddy/dma-cli.git
            ====================================================================
            `
            if (version != lastestVersion) {
                console.log('%s', chalk.green.bold(notify));
            }


        } catch (error) {
            //console.log("error",error)
            //Skip check version
        }




        return
    }
}