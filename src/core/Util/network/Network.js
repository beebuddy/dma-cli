import fs from "fs-extra";
import { AxiosRequestConfig } from "axios";
import CallAPI from "./CallAPI";
export default class Network {

    /**
     * 
     * @param {String} url 
     * @param {*} params 
     * @param {AxiosRequestConfig} config 
     */
    static async requestPost(url, params = {}, config = {}) {
        try {
            const mergeConfig = {
                // default timeout
                timeout: 30 * 60 * 1000, // 30 minutes
                ...config
            };
            const response = await CallAPI.post(url, params, mergeConfig);
            return response.data;
        } catch (error) {
            const { status, data, message } = error;
            if (status === 409) {
                throw data;
            } else {
                throw message ? message : error;
            }
        }
    }

    /**
     * 
     * @param {String} urlWithParams 
     * @param {AxiosRequestConfig} config 
     */
    static async requestGet(urlWithParams, config = {}) {
        try {
            const mergeConfig = {
                // default timeout
                timeout: 30 * 60 * 1000, // 30 minutes
                ...config
            };
            const response = await CallAPI.get(urlWithParams, mergeConfig);
            return response.data;
        } catch (error) {
            const { status, data, message } = error;
            if (status === 409) {
                throw data;
            } else {
                throw message ? message : error;
            }
        }
    }

    /**
     * 
     * @param {String} url 
     * @param {String} fileOutput
     * @param {*} params 
     * @param {AxiosRequestConfig} config 
     */
    static async requestDownloadPost(url, fileOutput, params = {}, config = {}) {
        try {
            const mergeConfig = {
                responseType: "stream",
                ...config
            }

            let resultDownload = await this.requestPost(url, params, mergeConfig);

            let downloadFileData = fs.createWriteStream(fileOutput);

            await new Promise((resolve, reject) => {
                resultDownload.pipe(downloadFileData);
                let error = null;
                downloadFileData.on("error", (error) => {
                    error = err;
                    downloadFileData.close();
                    reject(error);
                });
                downloadFileData.on('close', () => {
                    if (!error) {
                        resolve(true);
                    }
                });
            });

            return true;

        } catch (error) {
            throw error;
        }
    }

    /**
     * 
     * @param {String} url 
     * @param {String} fileOutput 
     * @param {AxiosRequestConfig} config 
     */
    static async requestDownloadGet(url, fileOutput, config = {}) {
        try {
            const mergeConfig = {
                responseType: "stream",
                ...config
            }

            let resultDownload = await this.requestGet(url, mergeConfig);

            let downloadFileData = fs.createWriteStream(fileOutput);

            await new Promise((resolve, reject) => {
                resultDownload.pipe(downloadFileData);
                let error = null;
                downloadFileData.on("error", (error) => {
                    error = err;
                    downloadFileData.close();
                    reject(error);
                });
                downloadFileData.on('close', () => {
                    if (!error) {
                        resolve(true);
                    }
                });
            });

            return true;

        } catch (error) {
            throw error;
        }
    }
}