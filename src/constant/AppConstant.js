const AppConstant = {
  API: {
    TOKEN: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6ImRlcGxveWVyMSIsInJvbGUiOiJkZXBsb3llciIsImNyZWF0ZV90aW1lIjoxNjA2ODgzNjIxODg2fQ._MYpKxTWQ7Y7w_nUeqSJsUY1jfaJiX9wMSfNI15EQx4",
    URL: {
      EXPORT_CARD: "https://dmastorecloudsit.beebuddy.net/cardgenerator/exportCustomCard"
    }
  },
};

export default AppConstant;