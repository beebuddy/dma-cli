# install dma-cli
npm install -g git+https://gitlab.com/beebuddy/dma-cli.git
# Initial project 
dma init
cd dma-card-project & npm install

# export card
dma export {cardName}

qrcode-dma-6.1-ios.png
qrcode-dma-6.1-android.png

