import 'react-native-gesture-handler';
import React from "react";
import TestTemplate from '~/cards/RenderCard';

const App = () => {
  return (
    <TestTemplate />
  );
}

export default App;
