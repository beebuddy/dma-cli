import { Dimensions } from 'react-native';
import DMAEvent from '~/core/event/DMAEvent'
import CardSubscriptionHook from './CardSubscriptionHook'

const calculatedDeviceScale = Dimensions.get("window").width / 375;

export const AddCardAPI = CardSubscriptionHook;

export const CardCallbackAction = async ({ props, actionName }) => {
    const callAction = props?.[actionName];
    if (callAction) {
        await callAction();
    }
};
class IDMA {

    /**
     * 
     * @param {String} componentId 
     * @param {String} apiName 
     * @param {Object} params 
     * @param {Number} timeout
     * @returns {Object}
     */
    static async callEvent(componentId, apiName, params, timeout = 10) {
        // submit event
        DMAEvent.emit(`${componentId}_request_${apiName}`, params);
        // subscribe for result
        return await new Promise((resolve, reject) => {
            let subscribeInstance = {};
            let timeoutInstance = setTimeout(() => {
                if (subscribeInstance) {
                    subscribeInstance.remove()
                }
                reject({ timeout: true, message: `Time out [${componentId}] "${apiName}"" is not responding` });
            }, timeout * 1000);

            subscribeInstance = DMAEvent.subscribe(`${componentId}_response_${apiName}`, (responseData) => {
                subscribeInstance.remove();
                clearTimeout(timeoutInstance);
                resolve(responseData);
            });
        });
    }

    /**
     * 
     * @returns {Number}
     */
    static getDeviceScale() {
        return calculatedDeviceScale;
    }

}

export default IDMA