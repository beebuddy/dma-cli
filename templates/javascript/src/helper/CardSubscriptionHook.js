import { useEffect, MutableRefObject } from 'react'
import DMAEvent from '~/core/event/DMAEvent';

/**
 * 
 * @param {Props} componentProps
 */
function CardSubscriptionHook(componentProps) {

  const { props, cardAPIName, triggerFunctionRef } = componentProps;
  const { _prefixCardName } = props;

  useEffect(() => {
    if (_prefixCardName && cardAPIName) {
      const eventName = `${_prefixCardName}_request_${cardAPIName}`;
      const subscriber = DMAEvent.subscribe(eventName, subscribeFunction);
      return () => subscriber.remove();
    }
  }, [_prefixCardName, cardAPIName]);

  const subscribeFunction = async (params) => {
    const triggerFunction = triggerFunctionRef?.current;
    if (triggerFunction) {
      const result = await triggerFunction(params);
      const eventName = `${_prefixCardName}_response_${cardAPIName}`;
      DMAEvent.emit(eventName, result);
    }
  }

  return null;
}

export default CardSubscriptionHook;

class Props {
  constructor() {
    this.props = {};
    this.cardAPIName = "";
    /**
     * @type {MutableRefObject}
     */
    this.triggerFunctionRef = {};
  }
}
